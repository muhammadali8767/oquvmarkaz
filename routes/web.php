<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'UnicatController@index')->name('index');
Route::get('/about', 'UnicatController@about')->name('about');
Route::get('/course', 'UnicatController@course')->name('course');
Route::get('/courses', 'UnicatController@courses')->name('courses');
Route::get('/blog', 'UnicatController@blog')->name('blog');
Route::get('/blog_single', 'UnicatController@blog_single')->name('blog_single');
Route::get('/contact', 'UnicatController@contact')->name('contact');

//Route::resource('tg-users','TelegramUserContorller');
//Route::resource('stadium','StadiumController');
//Route::resource('tg-users.stadium','StadiumController', ['only' => ['index', 'show']]);
//Route::resource('stadium.places', 'PlaceController');

/*Route::middleware(['auth'])->prefix('admin')->group(function () {
});*/

//Auth::routes([
//    'register' => false, // Registration Routes...
//    'reset' => false, // Password Reset Routes...
//    'verify' => false, // Email Verification Routes...
//]);

//Route::match(['get', 'post'], 'register', function(){
//    return redirect('/login');
//})->name('register');

// FormController routes
// Route::resource('forms','FormController');

// Route::middleware(['auth'])->prefix('admin')->namespace('Backend')->name('admin.')->group(function () {
//     Route::get('/', 'DashboardController@index')->name('index');
//     Route::get('/setting', 'SettingController@index')->name('setting.index');
//     Route::post('/setting/store', 'SettingController@store')->name('setting.store');
// });
