<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UnicatController extends Controller
{
    public function index () { return view('index'); }
    public function about () { return view('about'); }
    public function course () { return view('course'); }
    public function courses () { return view('courses'); }
    public function blog () { return view('blog'); }
    public function blog_single () { return view('blog_single'); }
    public function contact () { return view('contact'); }
}
